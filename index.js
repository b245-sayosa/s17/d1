// [SECTION] Functions
// functions in javascript are line/blocks of codes that tell our device/application to perform a specific task when called/invoked.
// it prevents repeating lines/blocks of codes that perform the same task/function.

// Function Declarations
 // function statement is the definition of a function.
/*
	Syntax:
		function functionName(){
			code block(statement)
		}

	-function keyword - used to define a javascript function.\
	-functionName - name of the function, which will be used to call/invoked the function
	-function block({}) - indicates the function body
*/

function printName(){
	console.log("My name is John")
}

// Function invocation
	// This run/execute the code block inside the function
	printName();

	declaredFunction(); 
	// we cannot invoked a function that we have not declared/defined yet

	// [SECTION] Function Declarations vs Function Expressions

		// Function Declaration
			// function can be created by using function keyword and adding a function name
			// "saved for later used"
			// Declare functions can be hoisted, as long a function has been defined.
				// hoisting is JS behavior for certain variables and functions to run or use them before their declaration.

	
	function declaredFunction(){
		console.log("Hello World from declaredFunction()")
	};

	// Function Expression
		// A function can also be stored in a variable.

		/*

			Syntax:
			let/const variableName = funtion(){
				//code block(statement)
			}

			-function(){}- anonymous function, a func w/o a name.

		*/

	//variableFunction(); //error- func expression, being 

	let variableFunction = function(){
		console.log("Hello Again");
	}

	variableFunction();

	let funcExpression = function funcName(){
		console.log("Hello from the other side. ")
	}

	//funcName(); // not defined

	funcExpression();

	// reassign declared function expression to a new anonymous function
	
	declaredFunction = function(){
		console.log("updated declaredFunction");
	}

	declaredFunction();

	funcExpression = function(){
		console.log("updated funcExpression");
	}
	funcExpression();

	//we cannot re-assign a function expression initialized with const.

	const constantFunc = function(){
		console.log("Cannot be reassigned")
	}

	constantFunc();

	// constantFunc = function(){
	// 	console.log("Cannot be reassigned")
	// }
	// constantFunc();

// [SECTION] Function Scoping
	/*
		Scope is the accessibility (visibility) of variables

		JavaScript variables has 3 types of scope.
		1. global scope
		2. local/

	*/

	let globalVar = "Mr. Worldwide";
	// console.log(localVar)
	//Local Scope
		// Variables declared inside a curly bracket({}) can only be accessed locally.
	{
		// var localVar = "Armando Perez";
		let localVar = "Armando Perez";
	}

	console.log(globalVar);
	//console.log(localVar) // error cannot be accessed outside its code block

	//Function Scope
		// Each Function creates a new scope.
		// Variables defined inside a function are not accessible from outsid the function

	function showNames(){
		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Joey"

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);

	}

	showNames();

	// console.log(functionVar);

	function myNewFunc(){
		let name = "Jane";

		function nestedFunc(){
		let nestedName = "John"
		console.log(name);
	}
	// console.log(nestedName);
	nestedFunc();
}
myNewFunc();

let globalName = "Alex";

function myNewFunction2(){
	let nameInside = "Renz";
	console.log(globalName);
}
myNewFunction2();

// console.log(nameInside); //only accessible on the function scope.

// [SECTION] Using alert() and prompt()
	// alert() allows us to show small window at the top of our browser page to show information to our users.
	alert("Hello World"); //this will run immediately when the page reloads.

	//you can use alert() to show a message to the user from a later function invocation.

	// function showSampleAlert(){
	// 	alert("Hello, User");
	// }
	// showSampleAlert();
	// console.log("I will only log in the console when the alert is dismissed")

	// notes on the use of alert:
		//

	//prompt() allows us to show a small window at the top of the browser to gather user input.
	// The input from the prompt() will returned as a "String" once the user dismissed the window.

	/*
	Syntax: prompt("<dialogInString>")

	*/

	// let name = prompt("Enter your name: ");
	// let age = prompt("Enter your age: ");

	// console.log(typeof age);

	// console.log("Hello I am "+name+", and Iam "+age+" years old.")

	// let sampleNullPrompt = prompt("Do not enter anything")
	// console.log(sampleNullPrompt);

	function printWelcomeMessage(){
		let name = prompt("Enter your name: ")
		console.log("Hello, "+ name + "! Welcome to my page!");
	}
printWelcomeMessage();

// [SECTION] Function Naming Conventions
	// Function names should be definitive of the task it will perform. It usually contains a verb
		// also follows camelCase for naming the variables

		function getCourses(){
			let courses = ["Science 101", "Math 101", "English 101"];
			console.log(courses);
		}
		getCourses();

	// avoid generic names to avoid confusion with your code
		function get(){
			let name = "Jamie"
			console.log(name);
		}

		get();
	// AVOID! pointless and inappropriate function names example: foo, bar

		function foo(){
			console.log(25%5)
		}

		foo();


